#include <iostream>
#include <string>
#include "FBullCowGame.h"
//for ue4
using FText = std::string;
using int32 = int;

void PrintWelcome();
void PlayGame();
bool PlayAgain();
FText GetValidGuess();

FBullCowGame BCGame;

int main() {
	bool bPlayAgain = true;
	do {
		PrintWelcome();
		PlayGame();
		bPlayAgain = PlayAgain();
	} while (bPlayAgain);
	return 0;
}

void PrintWelcome() {
	std::cout << "Welcome to BullCow game!" << std::endl;
	std::cout << "You have to break the " << BCGame.GetHiddenWordLength() << " letters \nisogram code to open a magic box" << std::endl;
	return;
}

FText GetValidGuess() {
	EWordStatus Status = EWordStatus::INVALID;
	FText Guess = "";
	do {

		int32 CurrentTry = BCGame.GetCurrentTry();
		std::cout << "Try " << CurrentTry << ". Enter your guess: ";
		std::getline(std::cin, Guess);

		Status = BCGame.IsValidWord(Guess);
		switch (Status) {
		case EWordStatus::TOO_LONG:
			std::cout << "Word is too long, shoud be " << BCGame.GetHiddenWordLength() << " letters long\n";
			break;
		case EWordStatus::TOO_SHORT:
			std::cout << "Word is too short, shoud be " << BCGame.GetHiddenWordLength() << " letters long\n";
			break;
		case EWordStatus::NOT_ISOGRAM:
			std::cout << "Word is not an isogram, do not repeat letters\n";
			break;
		default:
			Status = EWordStatus::OK;
			return Guess;
		}
	} while (Status != EWordStatus::OK);
	return Guess;
}

void PlayGame() {
	BCGame.Reset();
	int32 MaxTries = BCGame.GetMaxTries();

	for (int32 i = 0; i < MaxTries; i++) {
		FString Guess = GetValidGuess();
		std::cout << "Your guess: " << Guess << std::endl;

		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);
		std::cout << "Bulls = " << BullCowCount.Bulls << std::endl;
		std::cout << "Cows = " << BullCowCount.Cows << std::endl <<std::endl;
		if (BullCowCount.Bulls == BCGame.GetHiddenWordLength()) {
			// game won
			std::cout << "You have won!\n";
			return;
		}
		else {
			std::cout << "Left: " << MaxTries + 1 - BCGame.GetCurrentTry() << " lives.\n";
			if (BCGame.GetCurrentTry() == MaxTries + 1) {
				//game lost
				std::cout << "You have lost. ";
			}
		}
	}
}

bool PlayAgain() {
	std::cout << "Do you want to play again? Y/N \n";
	FText Response = "";
	std::getline(std::cin, Response);
	return (Response[0] == 'Y') || (Response[0] == 'y');
}