#include "FBullCowGame.h"

FBullCowGame::FBullCowGame()
{
	Reset();
}
int32 FBullCowGame::GetMaxTries() const { return MyMaxTries; }

FString FBullCowGame::doSort(FString Word)
{
	return FString();
}

void FBullCowGame::SetMaxTries(int32 MaxTries) { this->MyMaxTries = MaxTries; }

bool FBullCowGame::IsIsogram(FString Word) const
{
	TMap <char, bool> LetterSeen;
	for (auto Letter : Word) {
		Letter = tolower(Letter);
		if (LetterSeen[Letter]) {
			return false;
		}
		LetterSeen[Letter] = true;
	}
}

int32 FBullCowGame::GetCurrentTry() const { return MyCurrentTry; }

void FBullCowGame::SetCurrentTry(int32 CurrentTry) { this->MyCurrentTry = CurrentTry; }

void FBullCowGame::Reset()
{
	MyHiddenWord = "cakes";
	SetCurrentTry(1);
	SetMaxTries(MyHiddenWord.length()+3);
}


EWordStatus FBullCowGame::IsValidWord(FString Word) const
{
	if (Word.length() < GetHiddenWordLength()) { return EWordStatus::TOO_SHORT; }
	if (Word.length() > GetHiddenWordLength()) { return EWordStatus::TOO_LONG; }
	if (!IsIsogram(Word)) { return EWordStatus::NOT_ISOGRAM; }
	return EWordStatus::OK;
}

int32 FBullCowGame::GetHiddenWordLength() const
{
	return MyHiddenWord.length();
}

FBullCowCount FBullCowGame::SubmitValidGuess(FString Word)
{
	MyCurrentTry++;
	FBullCowCount BullCowCount;

	for (int32 i = 0; i < GetHiddenWordLength(); i++) {
		for (int32 j = 0; j < GetHiddenWordLength(); j++) {
			if (Word[i] == MyHiddenWord[j]) {
				if (i == j) {
					BullCowCount.Bulls++;
				}
				BullCowCount.Cows++;
			}
		}
	}
	return BullCowCount;
}




