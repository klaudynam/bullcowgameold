#pragma once
#include <string>
#include <map>
#define TMap std::map

using FString = std::string;
using FText = std::string;
using int32 = int;

struct FBullCowCount {
	int32 Bulls = 0;
	int32 Cows = 0;
};

enum class EWordStatus
{
	INVALID,
	OK,
	TOO_SHORT,
	TOO_LONG,
	NOT_ISOGRAM
};

class FBullCowGame {
public:
	FBullCowGame();

	void Reset();
	int32 GetMaxTries() const;
	FString doSort(FString Word);
	int32 GetCurrentTry() const;
	EWordStatus IsValidWord(FString Word) const;
	int32 GetHiddenWordLength() const;
	void SetCurrentTry(int32 CurrentTry);
	FBullCowCount SubmitValidGuess(FString Word);

private:
	int32 MyCurrentTry;
	int32 MyMaxTries;
	void SetMaxTries(int32 MaxTries);
	bool IsIsogram(FString Word) const;
	FString MyHiddenWord;
};